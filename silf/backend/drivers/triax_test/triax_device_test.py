import unittest

from silf.backend.drivers.triax.triax_190_device import Triax190Device
from silf.backend.commons.util.config import prepend_current_dir_to_config_file

class TestHioki3238Device(unittest.TestCase):
    def test_device_working(self):
        self.device = Triax190Device()
        self.device.init(prepend_current_dir_to_config_file("triax_config.ini"))
        self.do_test()

    def test_mock_working(self):
        self.device = Triax190Device()
        self.device.init(prepend_current_dir_to_config_file("mock_config.ini"))
        self.do_test()

    def do_test(self):
        slit = 500
        wave = 600
        self.device.power_up()
        self.device.start_measurements(slit)
        get_slit = self.device.triax_driver.get_slit_position()
        self.assertEqual(get_slit, slit)
        self.device.set_waveln(wave)
        get_wave = self.device.get_waveln()
        self.assertEqual(int(get_wave), wave)
        self.device.power_down()

if __name__ == '__main__':
    unittest.main()
