from silf.backend.commons.io.serialbase import SerialBase
import sys
from time import sleep
import typing

class Triax190Driver(SerialBase):

    def __init__(self, port: str, timeout: float, init_waveln: float = 275):
        """
        :param port: Serial port file name (e.g. /dev/ttyUSB0)
        :param timeout: Serial port read timeout:
            None: wait forever
            0: non-blocking mode, return immediately in any case, 
               returning zero or more, up to the requested number of bytes
            x: set timeout to x seconds (float allowed) returns immediately
               when the requested number of bytes are available,
               otherwise wait until the timeout expires and return all bytes that
               were received until then.
        :param init_waveln: initial wavelengh (after initialization of monochromator)
                            used for motor calibrate_motor_position
        """
        self.n_of_retries = 20
        self.retry = self.n_of_retries
        super().__init__(port=port, baud=9600, databits=8, stopbits=1, parity='N', timeout=timeout)
        self.init_waveln = init_waveln
        self.open_port()

    def send_bytes(self, b: typing.Union[int, str], if_read: typing.Optional[bool]=True) -> str:
        """
        Send byte 'b' to triax and return response if 'if_read' is true
        :param b: byte send to triax 
        :param if_read: if response should be read
        :return: response byte (if if_read == True)
        """
        ret = ''
        if isinstance(b, int):
            # int to char
            b = b.to_bytes(1, 'little')
        else:
            b = b.encode("ASCII")

        print('Write: ' +str(b))
        self.tty.flush()
        self.tty.write(b)
        if if_read:
            ret = self.tty.read()
            print ('Ret: ' + str(ret))
        self.tty.flush()
        return ret

    def init(self, comm: str=32):
        """
        Initialize the monochromator with recursively execution of this method
        (see manual - Spectrometer Control Manual - page 9).

        :param comm: initialization command send to monochromator,
                     default: "UTIL WHERE AM I" <SPACE> command
        """
        ret = self.send_bytes(comm)
        if ret == b'*':
            # Communications is established
            # Switch the monochromator controller to the intelligent communications mode
            return self.init(247)
        elif ret == b'\x1b':
            print("ESC")
            # Communications has been previously established in handheld
            # terminal communications mode.
            # Switch the controller to the intelligent communications mode
            # from the hand-held controller
            self.send_bytes(248, False)
            sleep(0.5)
            return self.init()
        elif ret == b'B':
            # We are in boot program
            # Switch the controller to MAIN program
            self.send_bytes('O2000\0', False)
            sleep(0.5)
            return self.init()
        elif ret == b'F':
            # Success - we are in MAIN program
            self.retry = self.n_of_retries
            return
        else:
            # REBOOT
            if self.retry:
                self.retry -= 1
                print('Retry: {}'.format(self. retry))
                return self.reboot()
            else:
                raise RuntimeError("Initialization of Triax failed due to timeout")

    def reboot(self):
        """
        Reboot of monochromator
        :return:  monochromator initialize method
        """
        # Switch the controller to the intelligent communications mode
        # from the hand-held controller
        self.send_bytes(248, False)
        sleep(0.5)
        # REBOOT
        self.send_bytes(222, False)
        sleep(0.5)
        self.close_port()
        self.open_port()
        sleep(1)
        return self.init()

    def init_motor(self):
        """
        MOTOR INIT
        Send "A" -> Receive "o"
        """
        self.send_bytes('A', False)
        for i in range(self.n_of_retries):
            print('Initialization of motor ...')
            sleep(2)
            if self.is_confirmed():
                return self.calibrate_motor_position()
        raise RuntimeError("Initialization of Motor failed due to timeout")

    def calibrate_motor_position(self):
        self.send_bytes('Z60,0,{}\r'.format(self.init_waveln), False)
        if self.is_confirmed():
            return True
        return False

    def get_wavelength(self)->float:
        """
        :return: current wavelength in nanmeters (float value)
        """
        self.send_bytes('Z62,0\r', False)
        if self.is_confirmed():
            ret = self.tty.readline()
            return float(ret)
        return False

    def set_wavelength(self, waveln: float):
        """
        Sets wavelength of TRIAX
        Send "Z61,0,546.074<CR>" -> Receive "o"
        :param waveln: wavelength
        :return: wait till motor is not busy
        """
        waveln = float(waveln)
        if waveln < 0: waveln = 0
        if waveln > 1400: waveln = 1400
        self.wait_if_motor_busy()
        self.send_bytes('Z61,0,{}\r'.format(waveln), False)
        if self.is_confirmed():
            try:
                return self.wait_if_motor_busy()
            except:
                raise RuntimeError("Setting of wavelengh {} failed - motor is busy".format(waveln))
        raise RuntimeError("Setting of wavelengh {} failed".format(waveln))

    def get_slit_position(self, slit: int=0):
        """
        SLIT READ POSITION
        Send "j0,0<CR>"
        Receive "o"
        Receive "100<CR>"
        :param slit: 0 - out (exit) slit
                     1 - in (entrance) slit
        :return: slit position
        """
        self.send_bytes('j0,{}\r'.format(int(slit)), False)
        if self.is_confirmed():
            ret = self.tty.readline()
            return int(ret)
        return False

    def set_slit_position(self, pos: int,  slit: int=0):
        """
        SLIT MOVE RELATIVE
        Send "k0,0,500<CR>" -> Receive "o"
        :param pos: position of the slit to set
        :param slit: 0 - out (exit) slit
                     1 - in (entrance) slit
        :return: wait till motor is not busy
        """
        pos = int(pos)
        if pos < 0:
            pos = 0
        if pos > 1500:
            pos = 1500
        current_pos = self.get_slit_position(slit)
        #position to relative position
        rel_pos = pos - current_pos
        if rel_pos == 0: return True
        self.wait_if_motor_busy()
        self.send_bytes('k0,{},{}\r'.format(slit, rel_pos), False)
        if self.is_confirmed():
            try:
                return self.wait_if_motor_busy()
            except:
                raise RuntimeError("Setting the position of slit {} to {} failed - motor is busy"
                                   .format(slit, pos))
        raise RuntimeError("Setting the position of slit {} to {} failed".format(slit, pos))

    def is_confirmed(self) -> typing.Optional[bool]:
        """
        :return: If command send to Triax is confirmed
        """
        ret = self.tty.read()
        if ret == b'o':
            return True
        print(ret)
        return False

    def is_motor_busy(self) -> typing.Optional[bool]:
        """
        :return: If the motor is busy - it does not finish work
        """
        self.tty.flush()
        self.send_bytes('E', False)
        if self.is_confirmed():
            ret = self.tty.read()
            if ret == b'q': return True
            if ret == b'z': return False

        raise RuntimeError("Checking if motor is busy failed")

    def wait_if_motor_busy(self) -> typing.Optional[bool]:
        """
        :return: True if the motor is not busy
        """
        for i in range(self.n_of_retries):
            sleep(2)
            if not self.is_motor_busy():
                return True
        raise RuntimeError("Motor is busy")

    def is_open(self) -> typing.Optional[bool]:
        """
        :return: If USB device is open
        """
        return self.tty and self.tty.isOpen()

# Test
if __name__ == "__main__":
    print (sys.argv[1])
    triax = Triax190Driver(sys.argv[1], 2)
    print (triax.is_open())
    triax.init()
    triax.init_motor()
    print(triax.get_wavelength())
    #print(triax.calibrate_motor_position())
    print(triax.get_wavelength())
    print(triax.set_wavelength(600))
    print(triax.get_wavelength())
    print(triax.set_slit_position(500, 0))
    print(triax.get_slit_position(0))
    print(triax.set_slit_position(1400, 1))
    print(triax.get_slit_position(1))
    sleep(10)
    print(triax.set_wavelength(600))
    print(triax.get_wavelength())
    triax.close_port()

