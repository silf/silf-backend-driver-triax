import sys
from time import sleep
import typing


class MockDriver():

    def __init__(self, init_waveln: float = 275):
        """
        Mock of moethod to initialize the monochromator
        """
        self.init_waveln = init_waveln
        self.waveln = self.init_waveln
        self.slit = 0
        sleep(2)
        return

    def init(self):
        """
        Initialize the monochromator with recursively execution of this methoda
        """
        sleep(2)
        return

    def reboot(self):
        """
        Mock of reboot
        """
        sleep(1)
        return self.init()

    def init_motor(self):
        """
        MOTOR INIT
        """
        sleep(2)
        return self.calibrate_motor_position()

    def calibrate_motor_position(self):
        self.waveln = self.init_waveln
        return True

    def get_wavelength(self)->float:
        """
        :return: current wavelength in nanmeters (float value)
        """
        return self.waveln

    def set_wavelength(self, waveln: float):
        """
        Sets wavelength of TRIAX
        :param waveln: wavelength
        :return: wait till motor is not busy
        """
        waveln = float(waveln)
        if waveln < 0:
            waveln = 0
        if waveln > 1400:
            waveln = 1400
        self.waveln = waveln
        return self.wait_if_motor_busy()

    def get_slit_position(self, slit: int=0):
        """
        SLIT READ POSITION
        :param slit: 0 - out (exit) slit
                     1 - in (entrance) slit
        :return: slit position
        """
        return self.slit

    def set_slit_position(self, pos: int,  slit: int=0):
        """
        SLIT MOVE RELATIVE
        :param pos: position of the slit to set
        :param slit: 0 - out (exit) slit
                     1 - in (entrance) slit
        :return: wait till motor is not busy
        """
        pos = int(pos)
        if pos < 0:
            pos = 0
        if pos > 1500:
            pos = 1500
        self.slit = pos
        return self.wait_if_motor_busy()

    def is_confirmed(self) -> typing.Optional[bool]:
        """
        :return: If command send to Triax is confirmed
        """
        return True

    def is_motor_busy(self) -> typing.Optional[bool]:
        """
        :return: If the motor is busy - it does not finish work
        """
        return True

    def wait_if_motor_busy(self) -> typing.Optional[bool]:
        """
        :return: True if the motor is not busy
        """
        return True

    def is_open(self) -> typing.Optional[bool]:
        """
        :return: If USB device is open
        """
        return True

    def close_port(self):
        pass

# Test
if __name__ == "__main__":
    triax = MockDriver()
    print (triax.is_open())
    triax.init()
    triax.init_motor()
    print(triax.get_wavelength())
    print(triax.get_wavelength())
    print(triax.set_wavelength(600))
    print(triax.get_wavelength())
    print(triax.set_slit_position(500, 0))
    print(triax.get_wavelength())
    triax.close_port()

