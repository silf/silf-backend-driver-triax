from silf.backend.drivers.triax.triax_190 import Triax190Driver
from silf.backend.drivers.triax.triax_mock import MockDriver
from silf.backend.commons.util.config import validate_mandatory_config_keys, open_configfile
import typing

class Triax190Device():
    device_id = 'Triax'
    live = False

    def init(self, config):
        print(config)
        device_id = self.device_id
        mandatory_config = ['filepath', 'timeout', 'init_waveln']
        self.config = open_configfile(config)

        if self.config is None:
            raise AssertionError("Config file required")

        print('', self.config, device_id, mandatory_config)
        validate_mandatory_config_keys(self.config, device_id, mandatory_config)

        if self.config.getboolean(device_id, 'use_mock', fallback=False):
            self.triax_driver = MockDriver()
        else:
            self.triax_driver = Triax190Driver(self.config[device_id]['filePath'],
                                               float(self.config[device_id]['timeout']))

    def power_up(self):
        if not self.triax_driver.is_open():
            self.triax_driver.open_port()
        self.triax_driver.init()
        self.triax_driver.init_motor()

    def start_measurements(self, slit: int=500):
        self.triax_driver.set_slit_position(slit, 0)

    def set_waveln(self, waveln: float):
        self.triax_driver.set_wavelength(waveln)

    def get_waveln(self) -> float:
        return self.triax_driver.get_wavelength()

    def power_down(self):
        self.triax_driver.set_slit_position(0, 0)
        print (self.triax_driver.is_open())
        self.triax_driver.close_port()




